package io.renren.modules.message.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.modules.message.entity.SysSmsEntity;

/**
 * 短信
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-22 16:01:07
 */
public interface SysSmsDao extends BaseMapper<SysSmsEntity> {
	
}
