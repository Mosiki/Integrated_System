/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.renren.modules.activiti.controller;

import io.renren.common.annotation.SysLog;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.R;
import io.renren.modules.activiti.form.ModelForm;
import io.renren.modules.activiti.service.ActModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 模型管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2018-07-15
 */
@RestController
@RequestMapping("/act/model")
public class ActModelController {
    @Autowired
    private ActModelService actModelService;

    /**
     * 列表
     */
    @RequestMapping("list")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        LayuiPage page = actModelService.queryPage(params);

        return page;
    }

    /**
     * 新增
     */
    @SysLog("新增模型")
    @RequestMapping("/save")
    public R save(@RequestBody ModelForm form) throws Exception{
        actModelService.save(form.getName(), form.getKey(), form.getDescription());

        return R.ok();
    }

    /**
     * 部署
     */
    @RequestMapping(value = "deploy")
    public R deploy(String id) {
        actModelService.deploy(id);
        return R.ok();
    }

    /**
     * 导出
     */
    @RequestMapping(value = "export")
    public void export(String id, HttpServletResponse response) {
        actModelService.export(id, response);
    }

    /**
     * 删除
     */
    @RequestMapping("delete")
    public R delete(@RequestBody String[] ids) {
        for(String id : ids) {
            actModelService.delete(id);
        }
        return R.ok();
    }
}
