package io.renren.modules.message.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-22 16:01:07
 */
@TableName("sys_sms")
public class SysSmsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 平台类型
	 */
	private Integer platform;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 参数1
	 */
	@TableField("params_1")
	private String params1;
	/**
	 * 参数2
	 */
	@TableField("params_2")
	private String params2;
	/**
	 * 参数3
	 */
	@TableField("params_3")
	private String params3;
	/**
	 * 参数4
	 */
	@TableField("params_4")
	private String params4;
	/**
	 * 发送状态  0：成功  1：失败
	 */
	private Integer status;
	/**
	 * 发送时间
	 */
	private Date sendTime;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台类型
	 */
	public void setPlatform(Integer platform) {
		this.platform = platform;
	}
	/**
	 * 获取：平台类型
	 */
	public Integer getPlatform() {
		return platform;
	}
	/**
	 * 设置：手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：手机号
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：参数1
	 */
	public void setParams1(String params1) {
		this.params1 = params1;
	}
	/**
	 * 获取：参数1
	 */
	public String getParams1() {
		return params1;
	}
	/**
	 * 设置：参数2
	 */
	public void setParams2(String params2) {
		this.params2 = params2;
	}
	/**
	 * 获取：参数2
	 */
	public String getParams2() {
		return params2;
	}
	/**
	 * 设置：参数3
	 */
	public void setParams3(String params3) {
		this.params3 = params3;
	}
	/**
	 * 获取：参数3
	 */
	public String getParams3() {
		return params3;
	}

	public String getParams4() {
		return params4;
	}

	public void setParams4(String params4) {
		this.params4 = params4;
	}

	/**
	 * 设置：发送状态  0：成功  1：失败
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：发送状态  0：成功  1：失败
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：发送时间
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getSendTime() {
		return sendTime;
	}
}
