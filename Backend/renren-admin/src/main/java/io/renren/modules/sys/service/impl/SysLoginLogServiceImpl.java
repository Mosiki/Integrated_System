package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.SysLoginLogDao;
import io.renren.modules.sys.entity.SysLoginLogEntity;
import io.renren.modules.sys.service.SysLoginLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysLoginLogService")
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogDao, SysLoginLogEntity> implements SysLoginLogService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String username = (String)params.get("username");
        String status = (String)params.get("status");

        Page<SysLoginLogEntity> page = this.selectPage(
                new Query<SysLoginLogEntity>(params).getPage("create_date", false),
                new EntityWrapper<SysLoginLogEntity>()
                    .like(StringUtils.isNotBlank(username),"username", username)
                    .eq(StringUtils.isNotBlank(status),"status", status)
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }

}
