package io.renren.modules.demo.service;


import com.alibaba.fastjson.JSONArray;

import java.util.Map;

public interface ChartsService {
    void callReportProcedure(String code);

    JSONArray callSPReturnJson(Map<String, Object> params);
}

