package io.renren.modules.message.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.Query;
import io.renren.modules.message.dao.SysMailLogDao;
import io.renren.modules.message.entity.SysMailLogEntity;
import io.renren.modules.message.service.SysMailLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;


@Service("sysMailLogService")
public class SysMailLogServiceImpl extends ServiceImpl<SysMailLogDao, SysMailLogEntity> implements SysMailLogService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String templateId = (String)params.get("templateId");
        String mailTo = (String)params.get("mailTo");
        String status = (String)params.get("status");

        Page<SysMailLogEntity> page = this.selectPage(
            new Query<SysMailLogEntity>(params).getPage("send_time", false),
            new EntityWrapper<SysMailLogEntity>()
                .eq(StringUtils.isNotBlank(templateId),"template_id", templateId)
                .eq(StringUtils.isNotBlank(status),"status", status)
                .like(StringUtils.isNotBlank(mailTo),"mail_to", mailTo)
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public void save(Long templateId, String from, String[] to, String[] cc, String subject, String content, Integer status) {
        SysMailLogEntity log = new SysMailLogEntity();
        log.setTemplateId(templateId);
        log.setMailFrom(from);
        log.setMailTo(JSON.toJSONString(to));
        if(cc != null){
            log.setMailCc(JSON.toJSONString(cc));
        }
        log.setSubject(subject);
        log.setContent(content);
        log.setStatus(status);
        log.setSendTime(new Date());
        this.insert(log);
    }

}
