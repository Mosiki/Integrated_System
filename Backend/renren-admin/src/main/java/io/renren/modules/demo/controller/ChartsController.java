package io.renren.modules.demo.controller;


import cn.hutool.core.lang.Console;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.utils.R;
import io.renren.modules.demo.service.ChartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("demo/charts2")
public class ChartsController {

    @Autowired
    private ChartsService chartsService;
    /**
     * 列表
     */
    @RequestMapping("/list")
//    public String getJsonStringBySP(@RequestParam Map<String, Object> params) {
//        //JSONObject strResult = null;
//        Console.log("88888888888888888");
//        Console.log(params);
//        JSONArray strResult = chartsService.callSPReturnJson(params);
//
//        Console.log(strResult);
//
//        return "";
//    }

    public R getJsonStringBySP(@RequestParam Map<String, Object> params) {
        Console.log("88888888888888888");
        Console.log(params);
        JSONArray jsonArrayResult = chartsService.callSPReturnJson(params);
        JSONArray jsonArrayResponse = getResponseData(jsonArrayResult);

        if (null == jsonArrayResponse) {
            return R.error("There is no result from excuting SP");
        }

        return R.ok().put("results", jsonArrayResponse);
    }


    /**
     * 得到JSONObject里的所有key
     * @param jsonArrayData JSONArray 实例对象
     * @return Set
     */
    private Set<String> getAllKeys(JSONArray jsonArrayData) {

        if (jsonArrayData.size() != 0) {
            return jsonArrayData.getJSONObject(0).keySet();
        }
        else {
            return null;
        }
    }

    /**
     * 将存储过程返回的结果，保存成如下格式，以便于前台调用
     * [{label: "HeadLine", label_data: [1,2,3,4,5]},{label: "AssemblyLine", label_data: [11,22,33,44,55]}]
     * @param jsonArrayData JSONArray 实例对象
     * @return JSONArray
     */
    private JSONArray getResponseData(JSONArray jsonArrayDataFromSP) {
        JSONArray jsonArrayResponse = new JSONArray();
        Set<String> keysResult = getAllKeys(jsonArrayDataFromSP);

        if (null == keysResult) {
            return null;
        }

        for (String ss : keysResult) {
            Console.log("The label is: " + ss);
            JSONObject jsonTemp = new JSONObject();
            jsonTemp.put("label", ss);
            JSONArray jsonArrayData = new JSONArray();
            for(int i = 0; i < jsonArrayDataFromSP.size(); i++) {
                JSONObject jsonObjectRows= jsonArrayDataFromSP.getJSONObject(i);
                jsonArrayData.add(jsonObjectRows.getString(ss));
            }
            jsonTemp.put("label_data", jsonArrayData);
            jsonArrayResponse.add(jsonTemp);
        }

        return jsonArrayResponse;
    }
}
