package io.renren.modules.demo.service.impl;

import cn.hutool.core.lang.Console;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.MyBatisUtil;

import io.renren.modules.demo.dao.NewsDao;
import io.renren.modules.demo.entity.NewsEntity;
import io.renren.modules.demo.service.ChartsService;
import org.activiti.explorer.reporting.Dataset;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.CallableStatement;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;



import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

@Service("ChartsService")
public class ChartsServiceImpl implements ChartsService {

//    @Autowired
//    protected ChartsDao chartsDao;

    @Override
    public void callReportProcedure(String storeCode) {

        try {
            SqlSession sqlSession = MyBatisUtil.getSqlSession();
            CallableStatement cs = sqlSession.getConnection().prepareCall("{call SP_getDictDetail(?,?,?)}");

            //一次给存储过程传递参数
            cs.setString(1,"abc");
            cs.setString(2,"def");
            //cs.setString(3, "ghi");
            cs.registerOutParameter(3, Types.VARCHAR);
            cs.execute();
            String totalCount = cs.getString(3);
            System.out.println("999999999 " + totalCount);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }


    @Override
    public JSONArray callSPReturnJson(Map<String, Object> params) {

        String spName = (String)params.get("spName");
        String strParams = (String)params.get("spParams");
        String[] parameters = strParams.split(",");
        JSONArray jsonArray = new JSONArray();

        try {
            SqlSession sqlSession = MyBatisUtil.getSqlSession();
            String prepareCallSQLpar = "";
            for (int i = 0; i < parameters.length; i++)
            {
                if (0 == i)
                {
                    prepareCallSQLpar = "?";
                } else {
                    prepareCallSQLpar = prepareCallSQLpar + ",?";
                }
            }
            String prepareCallSQL = "{call " + spName + "(" + prepareCallSQLpar + ")}";
            CallableStatement cs = sqlSession.getConnection().prepareCall(prepareCallSQL);
            for (int i = 0;  i< parameters.length; i++) {
                cs.setString(i+1,parameters[i]);
            }
            cs.execute();
            ResultSet rs = cs.getResultSet();

            // 获取列数
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            // json数组
            //JSONArray jsonArray = new JSONArray();

            // 遍历ResultSet中的每条数据
            while (rs.next()) {
                // json对象
                JSONObject jsonObj = new JSONObject();

                // 遍历每一列
                for (int i = 1; i <= columnCount; i++) {
                    String columnName =metaData.getColumnLabel(i);
                    String value = rs.getString(columnName);
                    jsonObj.put(columnName, value);
                }
                jsonArray.add(jsonObj);
            }
            Console.log("callSPReturnJson4:" + params);
            MyBatisUtil.closeSqlSession();

        } catch (Exception e) {
            MyBatisUtil.closeSqlSession();;
            e.printStackTrace();
        }

        return jsonArray;
    }








}
