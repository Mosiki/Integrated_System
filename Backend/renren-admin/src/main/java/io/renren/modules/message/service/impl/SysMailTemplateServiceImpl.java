package io.renren.modules.message.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import io.renren.common.exception.RRException;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.Query;
import io.renren.modules.message.dao.SysMailTemplateDao;
import io.renren.modules.message.email.EmailUtils;
import io.renren.modules.message.entity.SysMailTemplateEntity;
import io.renren.modules.message.service.SysMailTemplateService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysMailTemplateService")
public class SysMailTemplateServiceImpl extends ServiceImpl<SysMailTemplateDao, SysMailTemplateEntity> implements SysMailTemplateService {
    @Autowired
    private EmailUtils emailUtils;

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String name = (String)params.get("name");

        Page<SysMailTemplateEntity> page = this.selectPage(
            new Query<SysMailTemplateEntity>(params).getPage("create_date", false),
            new EntityWrapper<SysMailTemplateEntity>()
                .like(StringUtils.isNotBlank(name),"name", name)
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public boolean sendMail(Long templateId, String mailTo, String mailCc, String params) throws Exception{
        Map<String, Object> map = null;
        try {
            if(StringUtils.isNotEmpty(params)){
                map = JSON.parseObject(params, Map.class);
            }
        }catch (Exception e){
            throw new RRException("参数格式不正确，请使用JSON格式");
        }
        String[] to = new String[]{mailTo};
        String[] cc = StringUtils.isBlank(mailCc) ? null : new String[]{mailCc};

        return emailUtils.sendMail(templateId, to, cc, map);
    }
}
