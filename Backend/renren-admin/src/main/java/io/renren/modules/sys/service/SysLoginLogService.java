package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import io.renren.common.utils.LayuiPage;
import io.renren.modules.sys.entity.SysLoginLogEntity;

import java.util.Map;

/**
 * 登录日志
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-19 22:57:30
 */
public interface SysLoginLogService extends IService<SysLoginLogEntity> {

    LayuiPage queryPage(Map<String, Object> params);
}

