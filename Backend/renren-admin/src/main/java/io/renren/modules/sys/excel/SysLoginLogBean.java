package io.renren.modules.sys.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.util.Date;

/**
 * 登录日志
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-19 22:57:30
 */
public class SysLoginLogBean {
    @Excel(name = "ID")
    private Long id;
    @Excel(name = "用户名")
    private String username;
    @Excel(name = "用户操作")
    private String operation;
    @Excel(name = "IP地址")
    private String ip;
    @Excel(name = "状态", replace = { "登录成功_0", "登录失败_1", "账号已锁定_2"})
    private Integer status;
    @Excel(name = "登录时间", format = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }
    public void setOperation(String operation) {
        this.operation = operation;
    }
    public String getOperation() {
        return operation;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getIp() {
        return ip;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getStatus() {
        return status;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getCreateDate() {
        return createDate;
    }
}
