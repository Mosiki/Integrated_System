package io.renren.modules.message.controller;

import com.google.gson.Gson;
import io.renren.common.utils.ConfigConstant;
import io.renren.common.utils.LayuiPage;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.xss.XssHttpServletRequestWrapper;
import io.renren.modules.message.email.EmailConfig;
import io.renren.modules.message.entity.SysMailTemplateEntity;
import io.renren.modules.message.service.SysMailTemplateService;
import io.renren.modules.sys.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 邮件模板
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-23 12:35:17
 */
@RestController
@RequestMapping("sys/mailtemplate")
public class MailTemplateController {
    @Autowired
    private SysMailTemplateService sysMailTemplateService;
    @Autowired
    private SysConfigService sysConfigService;

    private final static String KEY = ConfigConstant.MAIL_CONFIG_KEY;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:mail:all")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        LayuiPage page = sysMailTemplateService.queryPage(params);

        return page;
    }

    /**
     * 邮件模板配置信息
     */
    @RequestMapping("/config")
    @RequiresPermissions("sys:sms:all")
    public R config(){
        EmailConfig config = sysConfigService.getConfigObject(KEY, EmailConfig.class);

        return R.ok().put("config", config);
    }

    /**
     * 保存邮件模板配置信息
     */
    @RequestMapping("/saveConfig")
    @RequiresPermissions("sys:mail:all")
    public R saveConfig(@RequestBody EmailConfig config){
        //校验数据
        ValidatorUtils.validateEntity(config);

        sysConfigService.updateValueByKey(KEY, new Gson().toJson(config));

        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:mail:all")
    public R info(@PathVariable("id") Long id){
        SysMailTemplateEntity sysMailTemplate = sysMailTemplateService.selectById(id);

        return R.ok().put("template", sysMailTemplate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:mail:all")
    public R save(HttpServletRequest request){
        HttpServletRequest orgRequest = XssHttpServletRequestWrapper.getOrgRequest(request);
        String name = orgRequest.getParameter("name");
        String subject = orgRequest.getParameter("subject");
        String content = orgRequest.getParameter("content");

        SysMailTemplateEntity template = new SysMailTemplateEntity();
        template.setName(name);
        template.setSubject(subject);
        template.setContent(content);
        template.setCreateDate(new Date());

        sysMailTemplateService.insert(template);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:mail:all")
    public R update(HttpServletRequest request){
        HttpServletRequest orgRequest = XssHttpServletRequestWrapper.getOrgRequest(request);
        String name = orgRequest.getParameter("name");
        String subject = orgRequest.getParameter("subject");
        String content = orgRequest.getParameter("content");
        String id = orgRequest.getParameter("id");

        SysMailTemplateEntity template = new SysMailTemplateEntity();
        template.setId(Long.parseLong(id));
        template.setName(name);
        template.setSubject(subject);
        template.setContent(content);

        sysMailTemplateService.updateById(template);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:mail:all")
    public R delete(@RequestBody Long[] ids){
        sysMailTemplateService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 发送邮件
     */
    @RequestMapping("/send")
    @RequiresPermissions("sys:mail:all")
    public R send(Long templateId, String mailTo, String mailCc, String params) throws Exception{
        boolean flag = sysMailTemplateService.sendMail(templateId, mailTo, mailCc, params);
        if(flag){
            return R.ok();
        }

        return R.error("邮件发送失败");
    }

}
