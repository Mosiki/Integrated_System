package io.renren.modules.message.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 邮件发送记录
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-23 12:35:17
 */
@TableName("sys_mail_log")
public class SysMailLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 邮件模板ID
	 */
	private Long templateId;
	/**
	 * 发送者
	 */
	private String mailFrom;
	/**
	 * 收件人
	 */
	private String mailTo;
	/**
	 * 抄送者
	 */
	private String mailCc;
	/**
	 * 邮件主题
	 */
	private String subject;
	/**
	 * 邮件正文
	 */
	private String content;
	/**
	 * 发送状态  0：成功  1：失败
	 */
	private Integer status;
	/**
	 * 发送时间
	 */
	private Date sendTime;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：邮件模板ID
	 */
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	/**
	 * 获取：邮件模板ID
	 */
	public Long getTemplateId() {
		return templateId;
	}
	/**
	 * 设置：发送者
	 */
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
	/**
	 * 获取：发送者
	 */
	public String getMailFrom() {
		return mailFrom;
	}
	/**
	 * 设置：接收者
	 */
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	/**
	 * 获取：接收者
	 */
	public String getMailTo() {
		return mailTo;
	}
	/**
	 * 设置：抄送者
	 */
	public void setMailCc(String mailCc) {
		this.mailCc = mailCc;
	}
	/**
	 * 获取：抄送者
	 */
	public String getMailCc() {
		return mailCc;
	}
	/**
	 * 设置：邮件主题
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * 获取：邮件主题
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * 设置：邮件正文
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：邮件正文
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：发送状态  0：成功  1：失败
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：发送状态  0：成功  1：失败
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：发送时间
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	/**
	 * 获取：发送时间
	 */
	public Date getSendTime() {
		return sendTime;
	}
}
