package io.renren.modules.message.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.modules.message.entity.SysMailTemplateEntity;

/**
 * 邮件模板
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-23 12:35:17
 */
public interface SysMailTemplateDao extends BaseMapper<SysMailTemplateEntity> {
	
}
