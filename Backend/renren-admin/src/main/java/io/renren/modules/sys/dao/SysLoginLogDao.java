package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysLoginLogEntity;

/**
 * 登录日志
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-07-19 22:57:30
 */
public interface SysLoginLogDao extends BaseMapper<SysLoginLogEntity> {
	
}
