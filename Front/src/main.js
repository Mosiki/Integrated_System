import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import '@/element-ui'
import '@/icons'
import '@/assets/scss/index.scss'
import http from '@/utils/http'
import { hasPermission } from '@/utils'
import cloneDeep from 'lodash/cloneDeep'
import VueI18n from 'vue-i18n'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'font-awesome/css/font-awesome.css'
import 'admin-lte/dist/css/AdminLTE.min.css'
import 'admin-lte/dist/css/skins/_all-skins.min.css'
Vue.config.productionTip = false
Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'zh-CN', // 语言标识
  messages: {
      'zh-CN': require('../static/lang/zh.js'), // 中文语言包
      'en-US': require('../static/lang/en.js') // 英文语言包
  }
})
// 挂载全局
Vue.prototype.$http = http
Vue.prototype.$hasPermission = hasPermission

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  store,
  components: { App },
  template: '<App/>'
})
