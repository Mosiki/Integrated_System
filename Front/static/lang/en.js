module.exports={
    message: {
      title: 'Intelligent Manufacturing',
      about: 'about',
      welcome: "Hello, welcome to use this system!",
      sysname: 'System Name',
      placeholderusername: "User Name",
      placeholderpassword: "Password",
      placeholdercaptcha: "Captcha",
      login: 'Login',
      updatepassword:'Update Password',
      logout:'Logout'
    },
    reportContent: {
        ElectricityTrend:'ElectricityTrend',
        EnergyConsumptionDashboard:'EnergyConsumptionDashboard',
        FirstQuarter:'FirstQuarter',
        SecondQuarter:'SecondQuarter',
        ThirdQuarter:'ThirdQuarter',
        FourthQuarter:'FourthQuarter'
    },
    menu : { 
        homepage:"Home Page" 
    }, 
    content:{ 
        main:"this is content" 
    }
  }