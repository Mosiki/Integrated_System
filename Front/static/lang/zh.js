module.exports={
    message: {
      title: '智能制造系统',
      about: '关于',
      welcome: "您好，欢迎使用该系统！",
      sysname: '系统名称',
      placeholderusername: "用户名",
      placeholderpassword: "密码",
      placeholdercaptcha: "验证码",
      login: '登录',
      updatepassword:'修改密码',
      logout:'退出'
    },
    reportContent: {
      ElectricityTrend:'电能趋势',
      EnergyConsumptionDashboard:'能源损耗看板',
      FirstQuarter:'第一季度',
      SecondQuarter:'第二季度',
      ThirdQuarter:'第三季度',
      FourthQuarter:'第四季度'
    },
    menu : { 
        homepage:"首页" 
    }, 
    content:{ 
        main:"这里是内容" 
    } 
}